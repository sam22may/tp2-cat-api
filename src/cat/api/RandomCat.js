const fetchRandomCat = async() => {
    const url = `https://api.thecatapi.com/v1/images/search?`
    const response = await fetch(url, {
        method: "GET",
        headers: {
            "x-api-key": "5f79fb88-7c39-4434-99ba-224ec9945ae8"
        }
    })
    const randomCat = await response.json()
    return randomCat
}

export { fetchRandomCat }